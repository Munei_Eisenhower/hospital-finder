package za.co.boloka.hospitalfinder.Objects;

/**
 * Created by Stackle_002 on 2016-03-04.
 */
import android.content.Intent;
import android.graphics.drawable.Drawable;

public class CropOption {
    public CharSequence title;
    public Drawable icon;
    public Intent appIntent;
}