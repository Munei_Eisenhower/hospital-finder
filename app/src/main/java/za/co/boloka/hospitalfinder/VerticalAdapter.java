package za.co.boloka.hospitalfinder;

/**
 * Created by Admin-123 on 2016-08-02.
 */
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

import za.co.boloka.hospitalfinder.Objects.MainItem;


/**
 * Created by Stackle_002 on 2016-07-18.
 */
public class VerticalAdapter extends RecyclerView.Adapter<VerticalAdapter.MyViewHolder> {
    HashMap<String,ArrayList<String>> listFromFire= new HashMap<>();
    private ArrayList<String> verticalList=new ArrayList<>();
    private ArrayList<String> distance=new ArrayList<>();
    private  ArrayList<MainItem> mainItemArrayList = new ArrayList<>();
    private  ArrayList<MainItem> mainItemArrayListFilrt = new ArrayList<>();

    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;
    Context context;
    int count=0;
    private FirebaseStorage storage ;
    StorageReference storageRef;
    private OnImageClickListener mOnImageClickListener;

    public interface OnImageClickListener {
        void onImageClick(int position,String json);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public de.hdodenhof.circleimageview.CircleImageView imgView;
        public LinearLayout linearLayout;
        public TextView textViewName;
        public  TextView textViewLoc;
        public  TextView textViewDistance;


        public MyViewHolder(View view) {
            super(view);

            imgView =(de.hdodenhof.circleimageview.CircleImageView)view.findViewById(R.id.imageViewDDImage);
textViewName =(TextView)view.findViewById(R.id.textViewDDName);
            textViewDistance=(TextView)view.findViewById(R.id.textViewMainDistance);
            textViewLoc =(TextView)view.findViewById(R.id.textViewDField);



linearLayout =(LinearLayout)view.findViewById(R.id.layer);

        }
    }


    //   public VerticalAdapter(Context context ,ArrayList<String> verticalList,HashMap<String,ArrayList<String>> listFromFire) {
    public VerticalAdapter(Context context,int count) {
        //   this.verticalList = verticalList;
        this.context =context;
        this.count =count;
        //    this.listFromFire =listFromFire;
    }


    public VerticalAdapter(){

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hospital_items, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int adapterPos = holder.getAdapterPosition();
                if(adapterPos != RecyclerView.NO_POSITION){
                    if (mOnImageClickListener != null) {
                        try {
                            mOnImageClickListener.onImageClick(adapterPos,mainItemArrayList.get(adapterPos).getNow());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }}
            }
        });

        MainItem mainItem =mainItemArrayList.get(position);
        holder.textViewName.setText(mainItem.getHospital_Name());
       //  if(distance.isEmpty()) {
            holder.textViewDistance.setText(mainItem.getHospital_Distance());
      //   }
       //  else{
        //    holder.textViewDistance.setText(distance.get(position));
      //  }
        holder.textViewLoc.setText(mainItem.getHospital_Address());


        //  holder.txtView.setText(verticalList.get(position));


     displayImageOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .resetViewBeforeLoading(true)
                .showImageOnFail(context .getResources().getDrawable(R.drawable.placeholder_image))
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.Builder(context)
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .build();
        imageLoader.init(configuration);
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://hospital-finder-72178.appspot.com");
        StorageReference islandRef3 = storageRef.child(mainItem.getHospital_Image());

        islandRef3.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'



                imageLoader.displayImage(uri.toString(), holder.imgView, displayImageOptions);
                Log.v("loading...", "from Net now");

                //   imageLoader.displayImage2(uri.toString(), imageView, displayImageOptions,folderPath );
                // Log.v("TestOND", uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                Log.v("TestOND", "failed");
            }
        });
      //imageLoader.displayImage(mainItem.getHospital_Image() , holder.imgView, displayImageOptions);


    }


    public void addData(ArrayList<String> images){
        this.verticalList.addAll(images);
        ///   notifyDataSetChanged();
    }
    public void addDistance(ArrayList<String> distance){
        this.distance.addAll(distance);

        MainItem item;
        for(int a=0;a<mainItemArrayList.size();a++){
            item= mainItemArrayList.get(a);
            item.setHospital_Distance(this.distance.get(a));
            mainItemArrayList.set(a,item);
            mainItemArrayListFilrt.set(a,item);

        }
      //  notifyDataSetChanged();
       Sortd();
    }

    public MainItem findWithName(String name) {
        MainItem item = null;
        Iterator<MainItem> iterator = mainItemArrayList.iterator();
        while(null == item && iterator.hasNext()) {
            MainItem potential = iterator.next();


        }
        return item;
    }

    public void Sortd(){
        sortIT();
        sortIT2();
      //  mainItemArrayListFilrt =mainItemArrayList;
        notifyDataSetChanged();
    }
    public  void sortIT(){
        Collections.sort(mainItemArrayList, new Comparator<MainItem>() {
            public int compare(MainItem v1, MainItem v2) {
              String d1 = v1.getHospital_Distance().replaceAll("\\D+","");
              String d2 = v2.getHospital_Distance().replaceAll("\\D+","");


                Log.v("Numbernow"," of:"+v1.getHospital_Distance()+" is "+d1);
                return d1.compareTo(d2);

            }
        });}
    public  void sortIT2(){
        Collections.sort(mainItemArrayListFilrt, new Comparator<MainItem>() {
            public int compare(MainItem v1, MainItem v2) {
                String d1 = v1.getHospital_Distance().replaceAll("\\D+","");
                String d2 = v2.getHospital_Distance().replaceAll("\\D+","");


                Log.v("Numbernow"," of:"+v1.getHospital_Distance()+" is "+d1);
                return d1.compareTo(d2);

            }
        });


    }
    public void addDistanceItem(String distance){

      //


        notifyDataSetChanged();
    }
    public void addData2(String images){
        this.verticalList.add(images);
        notifyDataSetChanged();
    }
    public void addDataItem(MainItem mainItems){
        this.mainItemArrayList.add(mainItems);
        this.mainItemArrayListFilrt.add(mainItems);
        notifyDataSetChanged();
    }

    public void addDataChild(HashMap<String,ArrayList<String>>images){
        this.listFromFire=images;
        //  notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mainItemArrayList.size();
    }

    public void setOnImageClickListener(OnImageClickListener listener) {
        this.mOnImageClickListener = listener;
    }

    // Do Search...
    public void filter(final String text) {

        // Searching could be complex..so we will dispatch it to a different thread...
        new Thread(new Runnable() {
            @Override
            public void run() {

                // Clear the filter list
                mainItemArrayList.clear();

                // If there is no search value, then add all original list items to filter list
                if (TextUtils.isEmpty(text)) {

                    mainItemArrayList.addAll(mainItemArrayListFilrt);

                } else {
                    // Iterate in the original List and add it to filter list...
                    for (MainItem item : mainItemArrayListFilrt) {
                        if (item.getHospital_Name().toLowerCase().contains(text.toLowerCase()) ||
                                item.getHospital_Address().toLowerCase().contains(text.toLowerCase())) {
                            // Adding Matched items
                           mainItemArrayList.add(item);
                        }
                    }
                }
//notifyDataSetChanged();
                // Set on UI Thread
               ((Activity) ((MyApplication)context).getMainAct()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Notify the List that the DataSet has changed...
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }
}