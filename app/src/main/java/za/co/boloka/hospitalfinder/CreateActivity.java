package za.co.boloka.hospitalfinder;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import za.co.boloka.hospitalfinder.Objects.UserItem;

public class CreateActivity extends AppCompatActivity {
    String TAG = "Create Acounnt";
    // [START declare_auth]
    private FirebaseAuth mAuth;

    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;
    private FirebaseStorage storage ;
    StorageReference storageRef;

    String name;
    String email;
    String joined;
    String UID;
    String password;
    private ProgressDialog pDialog;
    EditText editTextName;
    EditText editTextEmail;
    EditText editTextPass;
    EditText editTextPassAgain;
    AppCompatButton createButton;
    TextView textViewLogIn ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
editTextEmail = (EditText)findViewById(R.id.input_email);
editTextName = (EditText)findViewById(R.id.input_name);
editTextPass = (EditText)findViewById(R.id.input_password);
//editTextPassAgain = (EditText)findViewById(R.id.input_passwordAgain);
textViewLogIn =(TextView)findViewById(R.id.link_login);

createButton =(AppCompatButton)findViewById(R.id.btn_signup);
        //    Firebase.setAndroidContext(this);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        //final Firebase myFirebaseRef = new Firebase("https://blinding-inferno-2764.firebaseio.com/");
        mAuth = FirebaseAuth.getInstance();
        // [START auth_state_listener]
        storage = FirebaseStorage.getInstance();
        pDialog = new ProgressDialog(CreateActivity.this);
        storageRef = storage.getReferenceFromUrl("gs://hospital-finder-72178.appspot.com");
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
UID =user.getUid();
                    try {

                        Bitmap bitmap = getBitmap(CreateActivity.this, R.drawable.placeholder_image);

                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                        StorageReference userImagesRef = storageRef.child("User/"+user.getUid()+"/images/"+user.getUid()+".jpg");


                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] data = baos.toByteArray();

                        UploadTask uploadTask = userImagesRef.putBytes(data);
                        uploadTask.addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle unsuccessful uploads
                            }
                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                                Uri downloadUrl = taskSnapshot.getDownloadUrl();

                                writeNewUser(UID, email, name, taskSnapshot.getStorage().getPath(),"user");
                            }
                        });
                    }catch (Exception h){
                        h.printStackTrace();
                    }
                    //   myFirebaseRef.child("Users").child("Data").child(id).child("Profile_Picture").setValue(StringBitmap);








                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // [START_EXCLUDE]

                // [END_EXCLUDE]
            }
        };



        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setText();
            }
        });


        textViewLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    // [END on_start_add_listener]

    // [START on_stop_remove_listener]
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
        if (pDialog != null)
            pDialog.dismiss();
    }


    private void createAccount(String email, String password) {

         if (!validate()) {
             return;
          }


        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(CreateActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // [START_EXCLUDE]
                        pDialog.hide();
                        // [END_EXCLUDE]
                    }
                });
        // [END create_user_with_email]
    }

    public void setText() {
 if(validate()){
     name = editTextName.getText().toString();
     email = editTextEmail.getText().toString();
     password =editTextPass.getText().toString();

     createAccount(email,password);
 }else{
     return;
 }

    }

    private void writeNewUser(String userId, String email, String name, String image,String type  ) {
        UserItem user = new UserItem(email,name,image,type);



        Map<String, Object> postValues = user.toMap();

        Map<String, Object> childUpdates = new HashMap<>();

        childUpdates.put(userId, postValues);

        mDatabase.child("User").updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateActivity.this);
                    builder
                            .setTitle("Account Created Successful.")
                            .setMessage("Thank you for creating an account.\n " +
                                    "with Hospital finder \n" +
                                    "Press Ok to continue.")


                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                 //   return;

                                }
                            })
                            //Do nothing on no
                            .show();

                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateActivity.this);
                    builder
                            .setTitle("Creating Account was unsuccessful.")
                            .setMessage("Please try again letter.\n " +
                                    "with Hospital finder \n" +
                                    "Press Ok to continue.")


                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    return;

                                }
                            })
                            //Do nothing on no
                            .show();
                }

            }
        });


    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static Bitmap getBitmap(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof VectorDrawable) {
            return getBitmap((VectorDrawable) drawable);
        } else {
            throw new IllegalArgumentException("unsupported drawable type");
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static Bitmap getBitmap(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    public boolean validate() {
        boolean valid = true;

        String name = editTextName.getText().toString();
        String email = editTextEmail.getText().toString();
        String pass = editTextPass.getText().toString();
       // String passAgain = editTextPassAgain.getText().toString();

     if (name.isEmpty() || name.length() < 3) {
           editTextName.setError("at least 3 characters");
            valid = false;
        } else {
            editTextName.setError(null);
        }


        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextEmail.setError("enter a valid email address");
            valid = false;
        } else {
            editTextEmail.setError(null);
        }

        if (pass.isEmpty() || pass.length() < 6 ) {
            editTextPass.setError("at least 6 characters");
            valid = false;
        } else {
            editTextPass.setError(null);
        }
      //  if (!passAgain.equals(pass) ) {
      //      editTextPassAgain.setError("Password does not match");
      //      valid = false;
      //  } else {
      //      editTextPassAgain.setError(null);
       // }

        return valid;
    }


}
