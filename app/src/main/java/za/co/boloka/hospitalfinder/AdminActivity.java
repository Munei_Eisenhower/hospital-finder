package za.co.boloka.hospitalfinder;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import za.co.boloka.hospitalfinder.Objects.CropOption;

public class AdminActivity extends AppCompatActivity {
    PlacePicker.IntentBuilder builder;
    EditText editTextlocation;
    EditText editTextlocation_name;
    EditText editTexthospitalName;
    EditText editTexthospitalCoord;
    EditText editTexthospitalEmail;
    EditText editTexthospitalPhone;
    EditText editTexthospitalWeb;

    String hospitalName;
    String hospitalLocationName;
    String hospitalCoord;
    String hospitalEmail;
    String hospitalPhone;
    String hospitalWeb;
    String hospitalType;
    String hospitalImage="";
    ArrayList<String> myServices = new ArrayList<>();


    KeyListener variable;
    double locationgLat  ;
    double locationgLng  ;
    LatLng latLng ;
    Spinner spinner;
    int PIC_Upload_Logo=3;
    final int PIC_CROP = 2;
    private Uri fileUri;
    EditText editTextaddService;
    Button buttonAddService;
    int PLACE_PICKER_REQUEST = 11;
String UID;
    String storeHomeKey;
    private DatabaseReference mDatabase;
    private FirebaseStorage storage ;
    StorageReference storageRef;
    de.hdodenhof.circleimageview.CircleImageView proPic;
    ArrayAdapter<String> spinnerAdapter;
    Spinner typeSpinner;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Add Hospital");
        builder = new PlacePicker.IntentBuilder();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        //final Firebase myFirebaseRef = new Firebase("https://blinding-inferno-2764.firebaseio.com/");

        // [START auth_state_listener]
        storage = FirebaseStorage.getInstance();

        storageRef = storage.getReferenceFromUrl("gs://hospital-finder-72178.appspot.com");

        UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        typeSpinner = (Spinner) findViewById(R.id.static_spinner);
proPic =(de.hdodenhof.circleimageview.CircleImageView)findViewById(R.id.ImageHome);
        // Create an ArrayAdapter using the string array and a default spinner
        final ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(this, R.array.type_array,
                        android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        typeSpinner.setAdapter(staticAdapter);


        spinner = (Spinner)findViewById(R.id.services_spinner);
       spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, android.R.id.text1);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);

buttonAddService =(Button)findViewById(R.id.bt_addService) ;
        editTextaddService =(EditText)findViewById(R.id.txtaddServices) ;


        editTexthospitalName =(EditText)findViewById(R.id.txtName) ;
        editTextlocation_name =(EditText)findViewById(R.id.txtProvince) ;
        editTexthospitalEmail =(EditText)findViewById(R.id.txtEmail) ;
        editTexthospitalPhone =(EditText)findViewById(R.id.txtPhone) ;
        editTexthospitalWeb=(EditText)findViewById(R.id.txtWeb) ;
        editTexthospitalCoord =(EditText)findViewById(R.id.txtLocation) ;
        editTexthospitalName =(EditText)findViewById(R.id.txtName) ;
        editTextlocation = (EditText)findViewById(R.id.txtLocation);
        variable = editTextlocation.getKeyListener();
        editTextlocation.setKeyListener(null);

        editTextlocation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                if (action == MotionEvent.ACTION_DOWN) {
                    try {
                        startActivityForResult(builder.build(AdminActivity.this), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    }
                }
                return true;
            }
        });


        proPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(
                            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    //we will handle the returned data in onActivityResult
                    i.putExtra("return-data", true);
                    startActivityForResult(Intent.createChooser(i,
                            "Complete action using"), PIC_Upload_Logo);
                } catch (ActivityNotFoundException anfe) {
                    //display an error message
                    String errorMessage = "Whoops - your device doesn't support this!";
                    Toast toast = Toast.makeText(AdminActivity.this, errorMessage, Toast.LENGTH_SHORT);
                    toast.show();
                }





            }
        });

        buttonAddService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String serv = editTextaddService.getText().toString();
                spinnerAdapter.insert(serv,0);
                spinnerAdapter.notifyDataSetChanged();
            }
        });
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hospitalImage.length()<4) {
                    Snackbar.make(view, "Picture is not uploaded yet.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }else {
                    Snackbar.make(view, "Hospital added now wait to add Doctors.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    setText();

                }


            }
        });
    }


    public void setText(){
        if(!validate()){
            return;
        }else {
            hospitalName = editTexthospitalName.getText().toString();
            hospitalLocationName = editTextlocation_name.getText().toString();
            hospitalEmail = editTexthospitalEmail.getText().toString();
            hospitalPhone = editTexthospitalPhone.getText().toString();
            hospitalWeb = editTexthospitalWeb.getText().toString();
            hospitalCoord = editTexthospitalCoord.getText().toString();
            hospitalType = typeSpinner.getSelectedItem().toString();
            for (int p = 0; p < spinnerAdapter.getCount(); p++) {
                myServices.add(spinnerAdapter.getItem(p));
            }
            CreateHospital(storeHomeKey, hospitalName, hospitalLocationName, String.valueOf(latLng.latitude), String.valueOf(latLng.longitude), hospitalType.toLowerCase(), hospitalEmail, hospitalPhone, hospitalWeb, hospitalImage, myServices);
            Intent intent = new Intent(AdminActivity.this,AdminAddDoctor.class);

            intent.putExtra("Hospital_ID",storeHomeKey);
            startActivity(intent);

        }
    }

    public boolean validate() {
        boolean valid = true;

        String name = editTexthospitalName.getText().toString();
        String locations = editTexthospitalCoord.getText().toString();
        String locationName = editTextlocation_name.getText().toString();
        // String passAgain = editTextPassAgain.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            editTexthospitalName.setError("at least 3 characters");
            valid = false;
        } else {
            editTexthospitalName.setError(null);
        }
        if (locations.isEmpty() || locations.length() < 4) {
            editTexthospitalCoord.setError("touch inside to pick valid location");
            valid = false;
        } else {
            editTexthospitalCoord.setError(null);
        }
        if (locationName.isEmpty() || locationName.length() < 3) {
            editTextlocation_name.setError("at least 3 characters");
            valid = false;
        } else {
            editTextlocation_name.setError(null);
        }

    if (hospitalImage.isEmpty() || hospitalImage.length() < 3) {
        Toast.makeText(AdminActivity.this, "Please upload a picture", Toast.LENGTH_LONG).show();
        valid = false;
    }




        return valid;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (data != null) {

            if (requestCode == PLACE_PICKER_REQUEST) {
                if (resultCode == RESULT_OK) {
                    Place place = PlacePicker.getPlace(data, AdminActivity.this);

                    LatLng latLng = place.getLatLng();



                    editTextlocation.setKeyListener(variable);
                    editTextlocation.setText(latLng.latitude + "," + latLng.longitude);
                    this.latLng = latLng;
                    editTextlocation.setKeyListener(null);
                    //   Toast.makeText(getContext(), toastMsg, Toast.LENGTH_LONG).show();
                }
            }
            if (requestCode == PIC_Upload_Logo) {

                fileUri = data.getData();


                doCrop(PIC_Upload_Logo); //   performCrop();
            } else if (requestCode == PIC_CROP) {


                ArrayList<String> l = new ArrayList<String>();
                //    l.add(thePic.toString());
                //   pick.add(getPath(picUri));
                //  Toast.makeText(this, "pick size of:" +picUri+" is: "+ l.size(), Toast.LENGTH_LONG).show();


                Bundle extras = data.getExtras();

                final int extra1;

                if (extras != null) {
                    Bitmap photo = extras.getParcelable("data");

                    //        extra1 = extras.getInt("extra");

                    //      BitmapDrawable bdrawable = new BitmapDrawable(getContext().getResources(), photo);
                    //      (VectorDrawable) drawable
                    //     Canvas canvas = new Canvas(bitmap);
                    //     vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                    //     vectorDrawable.draw(canvas);


                    //    StorageReference logopathref = storageRef.child("business/" + key + "/");
                    //    String logoKey = mDatabase.child("logo").push().getKey();


                    proPic.setImageBitmap(photo);

                    // key2 =    mDatabase.child("mystores").push().getKey();
                       storeHomeKey = mDatabase.child("Hospitals").push().getKey();
                    StorageReference logopathref = storageRef.child("Hospitals/"+storeHomeKey+"/"+storeHomeKey+".jpg");
                    //      StorageReference logoRef = logopathref.child(storeHomeKey+".jpg");

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] dataFire = baos.toByteArray();

                    UploadTask uploadTask = logopathref.putBytes(dataFire);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Log.v("upload zwino","failedddd home:"+exception);
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                            Uri downloadUrl = taskSnapshot.getDownloadUrl();


                            hospitalImage =taskSnapshot.getStorage().getPath();
                            Log.v("upload zwino","kheyi home"+taskSnapshot.getStorage().toString());
                            Toast.makeText( AdminActivity.this, "Done Uploading you can click to Continue", Toast.LENGTH_SHORT).show();

                        }
                    });


                }
            }
        }

    }
    private void doCrop( int extra) {
        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        List<ResolveInfo> list =  this.getPackageManager().queryIntentActivities(intent, 0);

        int size = list.size();

        if (size == 0) {
            Toast.makeText( this, "Can not find image crop app", Toast.LENGTH_SHORT).show();


            return;
        } else {
            intent.setData(fileUri);

            intent.putExtra("outputX", 200);
            intent.putExtra("outputY", 200);
            intent.putExtra("aspectX", 0);
            intent.putExtra("aspectY", 0);
            intent.putExtra("scale", true);

            intent.putExtra("return-data", true);

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                try {
                    startActivityForResult(i, PIC_CROP);
                }catch (Exception e){
                    Toast.makeText(this, "Can not load image crop app", Toast.LENGTH_SHORT).show();
                }
            } else {
                try {
                    startActivityForResult(intent, PIC_CROP);
                }catch (Exception e){
                    Toast.makeText( this, "Can not load image crop apps", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    public void CreateHospital(String key,String name,String loctionName,String locationLati,String locationgLgni,String type,String email,String phone,String web,String image,ArrayList<String> services){

     try {
         DatabaseReference hosp = mDatabase.child("Hospitals").child(key);
         hosp.child("Name").setValue(name);
         hosp.child("image").setValue(image);
         hosp.child("Location_Name").setValue(loctionName);
         hosp.child("type").setValue(type);
         hosp.child("Contact").child("Phone").setValue(phone);
         hosp.child("Contact").child("email").setValue(email);
         hosp.child("Contact").child("website").setValue(web);
         hosp.child("Coordinates").child("lat").setValue(locationLati);
         hosp.child("Coordinates").child("lng").setValue(locationgLgni);
         for (String ser : services) {
             String sKey = mDatabase.child("services").push().getKey();
             hosp.child("services").child(sKey).child("name").setValue(ser);
         }
     }catch (Exception j){

         j.printStackTrace();
     return;
     }






    }

}
