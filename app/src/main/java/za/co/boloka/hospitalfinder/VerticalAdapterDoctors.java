package za.co.boloka.hospitalfinder;

/**
 * Created by Admin-123 on 2016-08-02.
 */
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;
import java.util.HashMap;

import za.co.boloka.hospitalfinder.Objects.DoctorItem;


/**
 * Created by Stackle_002 on 2016-07-18.
 */
public class VerticalAdapterDoctors extends RecyclerView.Adapter<VerticalAdapterDoctors.MyViewHolder> {
    HashMap<String,ArrayList<String>> listFromFire= new HashMap<>();
    private ArrayList<String> verticalList=new ArrayList<>();
    private  ArrayList<DoctorItem> doctorItemArrayList  = new ArrayList<>();
    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;
    Context context;
    private FirebaseStorage storage ;
    StorageReference storageRef;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public de.hdodenhof.circleimageview.CircleImageView imgView;
        public TextView textViewName;
        public TextView textViewField;
        public ImageView imageViewStatus;



        public MyViewHolder(View view) {
            super(view);
imgView =(de.hdodenhof.circleimageview.CircleImageView)view.findViewById(R.id.imageViewDDImage);
            textViewName=(TextView)view.findViewById(R.id.textViewDDName);
            textViewField=(TextView)view.findViewById(R.id.textViewDField);
            imageViewStatus =(ImageView)view.findViewById(R.id.textViewDStatus);


        }
    }


    //   public VerticalAdapter(Context context ,ArrayList<String> verticalList,HashMap<String,ArrayList<String>> listFromFire) {
    public VerticalAdapterDoctors(Context context) {
        //   this.verticalList = verticalList;
        this.context =context;

        //    this.listFromFire =listFromFire;
    }


    public VerticalAdapterDoctors(){

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.d, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        //  holder.txtView.setText(verticalList.get(position));

        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://hospital-finder-72178.appspot.com");
       displayImageOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .resetViewBeforeLoading(true)
                .showImageOnFail(context .getResources().getDrawable(R.drawable.placeholder_image))
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.Builder(context)
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .build();
        imageLoader.init(configuration);
        // imageLoader.displayImage(verticalList.get(position) , holder.imgView, displayImageOptions);

DoctorItem doctorItem = doctorItemArrayList.get(position);
        StorageReference islandRef3 = storageRef.child(doctorItem.getDoctorImage());

        islandRef3.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'



                imageLoader.displayImage(uri.toString(), holder.imgView, displayImageOptions);
                Log.v("loading...", "from Net now");

                //   imageLoader.displayImage2(uri.toString(), imageView, displayImageOptions,folderPath );
                // Log.v("TestOND", uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                Log.v("TestOND", "failed");
            }
        });
      //  imageLoader.displayImage(doctorItem.getDoctorImage() , holder.imgView, displayImageOptions);

        holder.textViewName.setText(doctorItem.getDoctorName());
        holder.textViewField.setText(doctorItem.getDoctorField());
        if(doctorItem.getDoctorStatus().equals("yes")){
            holder.imageViewStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.check));
        }else{
            holder.imageViewStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.close));
        }


    }
    public void addData(ArrayList<String> images){
        this.verticalList.addAll(images);
        ///   notifyDataSetChanged();
    }
    public void addData2(String images){
        this.verticalList.add(images);
        notifyDataSetChanged();
    }
    public void addDataItem(DoctorItem images){
        this.doctorItemArrayList.add(images);
        notifyDataSetChanged();
    }
    public void addDataChild(HashMap<String,ArrayList<String>>images){
        this.listFromFire=images;
        //  notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return doctorItemArrayList.size();
    }
}