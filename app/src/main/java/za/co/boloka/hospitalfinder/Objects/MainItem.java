package za.co.boloka.hospitalfinder.Objects;

import org.json.JSONObject;

/**
 * Created by Admin-123 on 2016-08-03.
 */
public class MainItem {
    String hospital_ID;
    String hospital_Name;
    String hospital_Address;
    String hospital_Distance;
    String hospital_Image;
    String hospital_Lat;
    String hospital_Lgn;
    String hospital_type;
    String hospital_email;
    String hospital_phone;
    String hospital_web;
    public MainItem(String hospital_ID,String hospital_Name,String hospital_Address,String hospital_Image,String hospital_Lat,String hospital_Lgn,String hospital_Distance,String hospital_type,String hospital_phone,String hospital_email,String hospital_web) {
          this.hospital_ID =hospital_ID;
        this.hospital_Name =hospital_Name;
        this.hospital_Address =hospital_Address;
        this.hospital_Image =hospital_Image;
        this.hospital_Lat =hospital_Lat;
        this.hospital_Lgn =hospital_Lgn;
        this.hospital_Distance =hospital_Distance;
        this.hospital_type =hospital_type;
        this.hospital_phone=hospital_phone;
        this.hospital_email =hospital_email;
        this.hospital_web =hospital_web;

    }

    public String getHospital_email() {
        return hospital_email;
    }

    public String getHospital_phone() {
        return hospital_phone;
    }

    public String getHospital_web() {
        return hospital_web;
    }

    public void setHospital_email(String hospital_email) {
        this.hospital_email = hospital_email;
    }

    public void setHospital_phone(String hospital_phone) {
        this.hospital_phone = hospital_phone;
    }

    public void setHospital_web(String hospital_web) {
        this.hospital_web = hospital_web;
    }

    public String getHospital_type() {
        return hospital_type;
    }

    public void setHospital_type(String hospital_type) {
        this.hospital_type = hospital_type;
    }

    public String getHospital_ID() {
        return hospital_ID;
    }

    public String getHospital_Name() {
        return hospital_Name;
    }

public String getNow()throws Exception
{

    JSONObject jsonObject = new JSONObject();
    jsonObject.put("hospital_ID",getHospital_ID());
    jsonObject.put("hospital_Name",getHospital_Name());
    jsonObject.put("hospital_Address",getHospital_Address());
    jsonObject.put("hospital_Image",getHospital_Image());
    jsonObject.put("hospital_Lat",getHospital_Lat());
    jsonObject.put("hospital_Lgn",getHospital_Lgn());
    jsonObject.put("hospital_distance",getHospital_Distance());
    jsonObject.put("hospital_type",getHospital_type());
    jsonObject.put("hospital_phone",getHospital_phone());
    jsonObject.put("hospital_email",getHospital_email());
    jsonObject.put("hospital_web",getHospital_web());
    return jsonObject.toString();

}

    public void  setAll(JSONObject jsonObject) throws Exception{
        setHospital_Name(jsonObject.getString("hospital_ID"));
        setHospital_ID(jsonObject.getString("hospital_Name"));
        setHospital_Address(jsonObject.getString("hospital_Address"));
        setHospital_Image(jsonObject.getString("hospital_Image"));
        setHospital_Lat(jsonObject.getString("hospital_Lat"));
        setHospital_Lgn(jsonObject.getString("hospital_Lgn"));
        setHospital_Distance(jsonObject.getString("hospital_distance"));
        setHospital_type(jsonObject.getString("hospital_type"));
        setHospital_phone(jsonObject.getString("hospital_phone"));
        setHospital_email(jsonObject.getString("hospital_email"));
        setHospital_web(jsonObject.getString("hospital_web"));
    }
    public String getAll(){
        String string ="{"+getHospital_ID()+","+getHospital_Name()+","+getHospital_Address()+","+getHospital_Image()+","+getHospital_Lat()+","+getHospital_Lgn()+","+getHospital_Distance();
        return  string;
    }
    public String getHospital_Address() {
        return hospital_Address;
    }

    public String getHospital_Distance() {
        return hospital_Distance;
    }

    public String getHospital_Image() {
        return hospital_Image;
    }

    public String getHospital_Lat() {
        return hospital_Lat;
    }

    public String getHospital_Lgn() {
        return hospital_Lgn;
    }

    public void setHospital_ID(String hospital_ID) {
        this.hospital_ID = hospital_ID;
    }

    public void setHospital_Name(String hospital_Name) {
        this.hospital_Name = hospital_Name;
    }

    public void setHospital_Address(String hospital_Address) {
        this.hospital_Address = hospital_Address;
    }

    public void setHospital_Distance(String hospital_Distance) {
        this.hospital_Distance = hospital_Distance;
    }

    public void setHospital_Image(String hospital_Image) {
        this.hospital_Image = hospital_Image;
    }

    public void setHospital_Lat(String hospital_Lat) {
        this.hospital_Lat = hospital_Lat;
    }

    public void setHospital_Lgn(String hospital_Lgn) {
        this.hospital_Lgn = hospital_Lgn;
    }

    @Override
    public String toString() {
        return "MainItem{" +
                "hospital_ID='" + hospital_ID + '\'' +
                ", hospital_Name='" + hospital_Name + '\'' +
                ", hospital_Address='" + hospital_Address + '\'' +
                ", hospital_Distance='" + hospital_Distance + '\'' +
                ", hospital_Image='" + hospital_Image + '\'' +
                ", hospital_Lat='" + hospital_Lat + '\'' +
                ", hospital_Lgn='" + hospital_Lgn + '\'' +
                '}';
    }
}
