package za.co.boloka.hospitalfinder;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import za.co.boloka.hospitalfinder.Objects.CropOption;
import za.co.boloka.hospitalfinder.Objects.UserItem;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    static TabLayout tabLayout;
     GPSTracker gpsTracker;
    final int PIC_Upload_Logo = 1;
    final int PIC_CROP = 2;
    private Uri fileUri;
  static boolean isOpen=false;
    de.hdodenhof.circleimageview.CircleImageView profile_pic;
TextView textViewName ;
TextView textViewEmail ;
    private DatabaseReference mDatabase;
    private FirebaseStorage storage ;
    StorageReference storageRef;
    String UID = "";
    public static final String MyPREFERENCES = "MyPrefs" ;
     public  static double lat=0.00;
  public static    double lgn =0.00;
    String email="";
    String name="";
    String user_pic="";
    Bitmap bitmapProPic=null;
    NavigationView navigationView;
    boolean isAdmin=true;
    SharedPreferences prefs;
    BaseActivity baseActivity = new BaseActivity();

    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        //final Firebase myFirebaseRef = new Firebase("https://blinding-inferno-2764.firebaseio.com/");
       prefs = this.getSharedPreferences(
               "za.co.boloka.hospitalfinder", Context.MODE_PRIVATE);
        // [START auth_state_listener]
        storage = FirebaseStorage.getInstance();



        storageRef = storage.getReferenceFromUrl("gs://hospital-finder-72178.appspot.com");
        Intent intent = getIntent();
        UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
       email = intent.getStringExtra("email");
isAdmin =intent.getBooleanExtra("isAdmin",false);
        if(!isAdmin) {
            isAdmin = prefs.getBoolean("isAdmin", false);
        }
        if(isAdmin){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.Red)));
        }else{
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.Blue)));
        }
       lat = intent.getDoubleExtra("lat",0.00);
      lgn = intent.getDoubleExtra("lgn",0.00);
        ((MyApplication) getApplication()).setMainAct(this);
        gpsTracker = new GPSTracker(this);
        if(lat==0.00 ||lgn==0.00){
            if(gpsTracker!=null) {
                if (!gpsTracker.isGPSEnabled) {
                    gpsTracker.showSettingsAlert();
                }
                if (!gpsTracker.canGetLocation) {
                    gpsTracker.showSettingsAlert();
                } else {
                    lat = gpsTracker.getLatitude();
                    lgn = gpsTracker.getLongitude();
                }
            }}
      /*  gpsTracker = new GPSTracker(this);
        if (!gpsTracker.isGPSEnabled) {
            gpsTracker.showSettingsAlert();
        } else {
            Toast.makeText(MainActivity.this, "lat:" + gpsTracker.getLatitude() + "-Long:" + gpsTracker.getLongitude(), Toast.LENGTH_SHORT).show();
        }*/



        Fragment fr = new Hospitals();


        FragmentManager fm = getFragmentManager();

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.fragment_place, fr);

        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

       navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if(isAdmin) {
            navigationView.setBackgroundColor(getResources().getColor(R.color.Red));
        }
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
isOpen =true;

            }

            @Override
            public void onDrawerClosed(View drawerView) {
isOpen=false;
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
   /*     if(isAdmin){
            navigationView.inflateMenu(R.menu.activity_main_draweradmin);
        }else{
            navigationView.inflateMenu(R.menu.activity_main_drawer);
        }*/

View view = navigationView.getHeaderView(0);

        textViewName =(TextView)view.findViewById(R.id.user_name);
        textViewEmail =(TextView)view.findViewById(R.id.userEmail);

        textViewName.setText("Test");
        profile_pic =(de.hdodenhof.circleimageview.CircleImageView)view.findViewById(R.id.imageViewPropic);

        displayImageOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .resetViewBeforeLoading(true)
                .showImageOnFail(getResources().getDrawable(R.drawable.placeholder_image))
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.Builder(MainActivity.this)
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .build();
        imageLoader.init(configuration);


        if(UID==null){
        FirebaseAuth firebaseAuth =FirebaseAuth.getInstance();
    UID=    firebaseAuth.getCurrentUser().getUid();}


        mDatabase.child("User").child(UID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                UserItem userItem = dataSnapshot.getValue(UserItem.class);
                if(userItem.getEmail()!=null) {
                    email = userItem.getEmail();
                }
                if(userItem.getName()!=null) {
                    name = userItem.getName();
                }
                if(userItem.getImage()!=null) {
                    user_pic = userItem.getImage();
                }
if(dataSnapshot.hasChild("type")) {
    if (dataSnapshot.child("type").getValue().toString().equals("admin")) {
        isAdmin = true;
        prefs.edit().putBoolean("isAdmin",true).commit();
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.Red)));
    } else {
        isAdmin = false;
        prefs.edit().putBoolean("isAdmin",false).commit();
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.Blue)));
    }
}else{
    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.Blue)));
    isAdmin=false;
    prefs.edit().putBoolean("isAdmin",false).commit();
}

                textViewName.setText(name);
                textViewEmail.setText(email);
                if(isAdmin){
                    navigationView.inflateMenu(R.menu.activity_main_draweradmin);
                }else{
                    navigationView.inflateMenu(R.menu.activity_main_drawer);
                }
                StorageReference userpicRef = storageRef.child(user_pic);

                userpicRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
        @Override
        public void onSuccess(Uri uri) {

            imageLoader.displayImage( uri.toString(), profile_pic, displayImageOptions);

        }
    }).addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception exception) {
            // Handle any errors
        }
    });






            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("boloka", "getUser:onCancelled", databaseError.toException());
            }
        });

    profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(
                            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    //we will handle the returned data in onActivityResult
                    i.putExtra("return-data", true);
                    startActivityForResult(Intent.createChooser(i,
                            "Complete action using"), PIC_Upload_Logo);
                } catch (ActivityNotFoundException anfe) {
                    //display an error message
                    String errorMessage = "Whoops - your device doesn't support this!";
                    Toast toast = Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT);
                    toast.show();
                }





            }
        });

    }




    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data!=null) {


            if (requestCode == PIC_Upload_Logo) {

                fileUri = data.getData();


                doCrop(PIC_Upload_Logo); //   performCrop();
            } else if (requestCode == PIC_CROP) {


                ArrayList<String> l = new ArrayList<String>();
                //    l.add(thePic.toString());
                //   pick.add(getPath(picUri));
                //  Toast.makeText(this, "pick size of:" +picUri+" is: "+ l.size(), Toast.LENGTH_LONG).show();


                Bundle extras = data.getExtras();

                final int extra1;

                if (extras != null) {
                    Bitmap photo = extras.getParcelable("data");

            //        extra1 = extras.getInt("extra");

              //      BitmapDrawable bdrawable = new BitmapDrawable(getContext().getResources(), photo);
                    //      (VectorDrawable) drawable
                    //     Canvas canvas = new Canvas(bitmap);
                    //     vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                    //     vectorDrawable.draw(canvas);


                //    StorageReference logopathref = storageRef.child("business/" + key + "/");
                //    String logoKey = mDatabase.child("logo").push().getKey();


                    profile_pic.setImageBitmap(photo);

                    // key2 =    mDatabase.child("mystores").push().getKey();
                    //   String storeHomeKey = mDatabase.child("store_image").push().getKey();
                      StorageReference logopathref = storageRef.child("User/"+UID+"/images/"+UID+".jpg");
                    //      StorageReference logoRef = logopathref.child(storeHomeKey+".jpg");

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] dataFire = baos.toByteArray();

                      UploadTask uploadTask = logopathref.putBytes(dataFire);
                        uploadTask.addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                Log.v("upload zwino","failedddd home:"+exception);
                            }
                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                                Uri downloadUrl = taskSnapshot.getDownloadUrl();

                                Log.v("upload zwino","kheyi home"+taskSnapshot.getStorage().toString());


                            }
                        });


                }
            }
        }

    }
    private void doCrop( int extra) {
        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        List<ResolveInfo> list =  this.getPackageManager().queryIntentActivities(intent, 0);

        int size = list.size();

        if (size == 0) {
            Toast.makeText( this, "Can not find image crop app", Toast.LENGTH_SHORT).show();


            return;
        } else {
            intent.setData(fileUri);

            intent.putExtra("outputX", 200);
            intent.putExtra("outputY", 200);
            intent.putExtra("aspectX", 0);
            intent.putExtra("aspectY", 0);
            intent.putExtra("scale", true);

            intent.putExtra("return-data", true);

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                try {
                    startActivityForResult(i, PIC_CROP);
                }catch (Exception e){
                    Toast.makeText(this, "Can not load image crop app", Toast.LENGTH_SHORT).show();
                }
            } else {
                try {
                    startActivityForResult(intent, PIC_CROP);
                }catch (Exception e){
                    Toast.makeText( this, "Can not load image crop apps", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else  if (id == R.id.signout) {

            FirebaseAuth.getInstance().signOut();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
//        item.getIcon().setColorFilter(Color.parseColor("#b69260"), PorterDuff.Mode.SRC_ATOP);
        if (id == R.id.nav_hospital) {
            // Handle the camera action

            Hospitals hospitals = new Hospitals();

            selectFrag(hospitals);

            //  JSONParser jsonParser = new JSONParser();
            //  jsonParser.getJSONFromUrl("http://maps.googleapis.com/maps/api/directions/json?origin=Toronto&destination=Montreal&sensor=false&units=metric");
        } else if (id == R.id.nav_doctor) {

            Doctors doctors = new Doctors();

            selectFrag(doctors);

        } else if (id == R.id.nav_settings) {

            Settings settings = new Settings();
            selectFrag(settings);
        } else if (id == R.id.nav_SignOut) {
            FirebaseAuth.getInstance().signOut();
            finish();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void selectFrag(Fragment f) {

        Fragment fr = f;


        FragmentManager fm = getFragmentManager();

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.fragment_place, fr);

        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();







    }

    public  void openAdminAdd(MenuItem v){
     Toast.makeText(MainActivity.this,"Welcome Admin",Toast.LENGTH_SHORT).show();
        startActivity(new Intent(MainActivity.this,AdminActivity.class));

    }


    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {


            return getDistance(gpsTracker.getLatitude(), gpsTracker.getLongitude(), lat, lgn);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            Log.i("weAre",result);
        }


    }

    public String getDistance(double lat1, double lon1, double lat2, double lon2) {
        String distance = "";
        System.out.println(lat1 + " " + lon1 + " " + lat2 + " " + lon2);
        String url = "http://maps.google.com/maps/api/directions/xml?origin=" + lat1 + "," + lon1 + "&destination=" + lat2 + "," + lon2 + "&sensor=false&units=metric";
        String tag[] = {"text"};  //will give distance as string e.g 1.2 km
// or tag[] = {"value"} if you want to get distance in metre e.g. 1234

        HttpResponse response = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            HttpPost httpPost = new HttpPost(url);
            response = httpClient.execute(httpPost, localContext);
            InputStream is = response.getEntity().getContent();
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(is);
            if (doc != null) {
                NodeList nl;
                ArrayList args = new ArrayList();
                for (String s : tag) {
                    nl = doc.getElementsByTagName(s);
                    if (nl.getLength() > 0) {
                        Node node = nl.item(nl.getLength() - 1);
                        args.add(node.getTextContent());
                    } else {
                        args.add(" - ");
                    }
                }
                distance = String.format("%s", args.get(0));
            } else {
                System.out.print("Doc is null");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return distance;
    }

}
