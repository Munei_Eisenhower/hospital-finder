package za.co.boloka.hospitalfinder;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import za.co.boloka.hospitalfinder.Objects.MainItem;

/**
 * Created by Admin-123 on 2016-08-02.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Hospitals extends Fragment implements VerticalAdapter.OnImageClickListener , SearchView.OnQueryTextListener  {
    String UID = "";
    double lat=new MainActivity().lat;
    double lgn =new MainActivity().lgn;
    String email="";
    String name="";
    String user_pic="";
    ArrayList<Location> locations = new ArrayList<>();
    ArrayList<String> distance = new ArrayList<>();
    RecyclerView mRecyclerView ;
    VerticalAdapter verticalAdapter;
    ArrayList<MainItem> mainItemArrayList = new ArrayList<>();
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;
    private FirebaseStorage storage ;
    StorageReference storageRef;
    GPSTracker gpsTracker;
    DatabaseReference hospitalsRef;
    SearchView searchView;
    ChildEventListener childEventListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_hospitals, container, false);

        mRecyclerView =(RecyclerView)view.findViewById(R.id.recycler_view);
        setHasOptionsMenu(true);
        verticalAdapter =new VerticalAdapter(getActivity().getApplicationContext(),10);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        gpsTracker = new GPSTracker(getActivity().getApplicationContext());
        verticalAdapter.setOnImageClickListener(this);
        mRecyclerView.setLayoutManager(horizontalLayoutManagaer);
        mRecyclerView.setAdapter(verticalAdapter);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        searchView =(SearchView)view.findViewById(R.id.search_view) ;
        //final Firebase myFirebaseRef = new Firebase("https://blinding-inferno-2764.firebaseio.com/");

        // [START auth_state_listener]
        storage = FirebaseStorage.getInstance();

        storageRef = storage.getReferenceFromUrl("gs://hospital-finder-72178.appspot.com");



        if(lat==0.00 ||lgn==0.00){
            if(gpsTracker!=null) {
                if (!gpsTracker.isGPSEnabled) {
                    gpsTracker.showSettingsAlert();
                }
                if (!gpsTracker.canGetLocation) {
                    gpsTracker.showSettingsAlert();
                } else {
                    lat = gpsTracker.getLatitude();
                    lgn = gpsTracker.getLongitude();
                }
            }}

        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setSubmitButtonEnabled(true);
        searchView.setQueryHint("Search Here");

      hospitalsRef = mDatabase.child("Hospitals");



    hospitalsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {


                for(DataSnapshot dataSnapshot :ds.getChildren()) {

                    MainItem mainItem;
                    Location location;

                    location = new Location("");
                    location.setLatitude(Double.parseDouble(dataSnapshot.child("Coordinates").child("lat").getValue().toString()));
                    location.setLongitude(Double.parseDouble(dataSnapshot.child("Coordinates").child("lng").getValue().toString()));
                    mainItem = new MainItem(dataSnapshot.getKey(), dataSnapshot.child("Name").getValue().toString(), dataSnapshot.child("Location_Name").getValue().toString(), dataSnapshot.child("image").getValue().toString(), dataSnapshot.child("Coordinates").child("lat").getValue().toString(), dataSnapshot.child("Coordinates").child("lng").getValue().toString(), "", dataSnapshot.child("type").getValue().toString(), dataSnapshot.child("Contact").child("Phone").getValue().toString(), dataSnapshot.child("Contact").child("email").getValue().toString(), dataSnapshot.child("Contact").child("website").getValue().toString());
                    verticalAdapter.addDataItem(mainItem);
                    locations.add(location);
                }
//new HttpAsyncTaskDistance().execute(location.getAltitude(),location.getLongitude());
                new HttpAsyncTask().execute("");
                //
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();


    }

    @Override
    public void onImageClick(int position,String json) {
        Intent intent = new Intent(getActivity().getApplicationContext(),HospitalDetails.class);


        intent.putExtra("position", position);
        intent.putExtra("data", json);

        startActivity(intent);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {



        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
      // Toast.makeText(getActivity(),newText,Toast.LENGTH_SHORT).show();

        verticalAdapter.filter(newText);
        return true;
    }


    private class HttpAsyncTaskDistance extends AsyncTask<Double,Void,String>{

        @Override
        protected String doInBackground(Double... params) {

            String dis="";
         dis=   getDistance(lat, lgn, params[0], params[1]);
            return dis;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            verticalAdapter.addDistanceItem(s);
        }
    }

   private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {


            for(int d=0;d<locations.size();d++){
           //     Log.i("distanceNow", "current:"+lat+":"+lgn +"to"+ locations.get(d).getLatitude()+":"+ locations.get(d).getLongitude());

    distance.add(getDistance(lat, lgn, locations.get(d).getLatitude(), locations.get(d).getLongitude()));


       //     Log.i("distanceNow",distance.get(d));
            }



            return "done";
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {


          verticalAdapter.addDistance(distance);

        }


    }





    public String getDistance(double lat1, double lon1, double lat2, double lon2) {
        String distance = "";
        System.out.println(lat1 + " " + lon1 + " " + lat2 + " " + lon2);
        String url = "http://maps.google.com/maps/api/directions/xml?origin=" + lat1 + "," + lon1 + "&destination=" + lat2 + "," + lon2 + "&sensor=false&units=metric";
        String tag[] = {"text"};  //will give distance as string e.g 1.2 km
// or tag[] = {"value"} if you want to get distance in metre e.g. 1234

        HttpResponse response = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            HttpPost httpPost = new HttpPost(url);
            response = httpClient.execute(httpPost, localContext);
            InputStream is = response.getEntity().getContent();
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(is);
            if (doc != null) {
                NodeList nl;
                ArrayList args = new ArrayList();
                for (String s : tag) {
                    nl = doc.getElementsByTagName(s);
                    if (nl.getLength() > 0) {
                        Node node = nl.item(nl.getLength() - 1);
                        args.add(node.getTextContent());
                    } else {
                        args.add(" - ");
                    }
                }
                distance = String.format("%s", args.get(0));
            } else {
                System.out.print("Doc is null");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return distance;
    }


}
