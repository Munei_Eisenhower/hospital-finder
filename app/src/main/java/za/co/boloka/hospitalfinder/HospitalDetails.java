package za.co.boloka.hospitalfinder;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.content.Intent;

import android.support.design.widget.TabLayout;

import android.support.v4.view.ViewPager;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import za.co.boloka.hospitalfinder.Objects.DoctorItem;
import za.co.boloka.hospitalfinder.Objects.MainItem;

public class HospitalDetails extends AppCompatActivity {

    static TabLayout tabLayout ;
    Intent intent;
    JSONObject jsonObject;
 ImageView imageView;
    TextView textViewName,textViewAddress,textViewDistance,textViewType,textViewEmail,textViewPhone,textViewWebsite;
String name="";
String address="";
String distance="";
String type="";
String email="";
String phone="";
String web="";
String img="";
String hospitalID="";
    double hospLat,hospLng;
    double currentLat=0.00;
    double currentLgn=0.00;
    GPSTracker gpsTracker;
    ArrayList<String>  serviceArrayList =new ArrayList<>();
    SharedPreferences prefs;
    private DatabaseReference mDatabase;
    private FirebaseStorage storage ;
    StorageReference storageRef;

    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;
    FloatingActionButton fabDirrection;
 static   VerticalAdapterDoctors  verticalAdapterDoctors;
  static  VerticalAdapterServices verticalAdapterServices;
     DatabaseReference doctorsRef;
  DatabaseReference hospitalServicesRef;
    ChildEventListener doctorschildEventListener;
    ChildEventListener serviceschildEventListener;
    boolean isAdmin=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        storage = FirebaseStorage.getInstance();
        prefs = this.getSharedPreferences(
                "za.co.boloka.hospitalfinder", Context.MODE_PRIVATE);

        isAdmin = prefs.getBoolean("isAdmin",false);

        if(isAdmin){
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.Red)));
        }else{
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.Blue)));
        }
        storageRef = storage.getReferenceFromUrl("gs://hospital-finder-72178.appspot.com");
        gpsTracker = new GPSTracker(getApplicationContext());
intent =getIntent();
        String json = intent.getStringExtra("data");

        try {
            jsonObject =new JSONObject(json);
            name = jsonObject.getString("hospital_Name");
            address = jsonObject.getString("hospital_Address");
           distance = jsonObject.getString("hospital_distance");
            type = jsonObject.getString("hospital_type");
            email = jsonObject.getString("hospital_email");
            phone = jsonObject.getString("hospital_phone");
            web = jsonObject.getString("hospital_web");
            img = jsonObject.getString("hospital_Image");
            hospitalID = jsonObject.getString("hospital_ID");
            hospLat =  Double.parseDouble(jsonObject.getString("hospital_Lat"));
            hospLng =Double.parseDouble(jsonObject.getString("hospital_Lgn"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        imageView = (ImageView)findViewById(R.id.imageViewDDImage);
        textViewName = (TextView)findViewById(R.id.textViewDDName);
        textViewAddress = (TextView)findViewById(R.id.textViewDField);
        textViewDistance = (TextView)findViewById(R.id.textViewDStatus);
        textViewType = (TextView)findViewById(R.id. hospitalType);
        textViewEmail = (TextView)findViewById(R.id. hospitalEmail);
        textViewPhone = (TextView)findViewById(R.id. hospitalPhone);
        textViewWebsite = (TextView)findViewById(R.id. hospitalWeb);

        textViewName.setText(name);
        textViewAddress.setText(address);
        textViewDistance.setText(distance);
        textViewEmail.setText(email);
        textViewPhone.setText(phone);
        textViewWebsite.setText(web);
        textViewType.setText(type);

        displayImageOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .resetViewBeforeLoading(true)
                .showImageOnFail(this.getResources().getDrawable(R.drawable.placeholder_image))
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .build();
        imageLoader.init(configuration);

        StorageReference islandRef3 = storageRef.child(img );

        islandRef3.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'



                imageLoader.displayImage(uri.toString(),  imageView, displayImageOptions);
                Log.v("loading...", "from Net now");

                //   imageLoader.displayImage2(uri.toString(), imageView, displayImageOptions,folderPath );
                // Log.v("TestOND", uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                Log.v("TestOND", "failed");
            }
        });


        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        if(isAdmin)
        tabLayout.setBackgroundColor(getResources().getColor(R.color.DarkCyan));

        // TabLayout t1 = new TabLayout( (getApplication().getApplicationContext()));
        // t1.newTab().setText("Best");
        tabLayout.addTab(tabLayout.newTab().setText("Services"));

        tabLayout.addTab(tabLayout.newTab().setText("Doctors"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        if(currentLat==0.00 ||currentLgn==0.00){
            if(gpsTracker!=null) {
                if (!gpsTracker.isGPSEnabled) {
                    gpsTracker.showSettingsAlert();
                }
                if (!gpsTracker.canGetLocation) {
                    gpsTracker.showSettingsAlert();
                } else {
                    currentLat = gpsTracker.getLatitude();
                    currentLgn = gpsTracker.getLongitude();
                }
            }}



        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        mDatabase = FirebaseDatabase.getInstance().getReference();
        //final Firebase myFirebaseRef = new Firebase("https://blinding-inferno-2764.firebaseio.com/");

        // [START auth_state_listener]


        verticalAdapterServices = new VerticalAdapterServices(this);





        hospitalServicesRef = mDatabase.child("Hospitals").child(hospitalID).child("services");


        hospitalServicesRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    verticalAdapterServices.addData2(dataSnapshot1.child("name").getValue().toString());
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


verticalAdapterDoctors = new VerticalAdapterDoctors(this);

        doctorsRef = mDatabase.child("Hospitals").child(hospitalID).child("Doctors");

        doctorsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot1) {
                for(DataSnapshot dataSnapshot:dataSnapshot1.getChildren()) {
                    DoctorItem doctorItem;

                    doctorItem = new DoctorItem(dataSnapshot.child("image").getValue().toString(), dataSnapshot.child("name").getValue().toString(), dataSnapshot.child("field").getValue().toString(), dataSnapshot.child("avaliable").getValue().toString(), name);
                    verticalAdapterDoctors.addDataItem(doctorItem);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {




                    viewPager.setCurrentItem(tab.getPosition());





            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        fabDirrection = (FloatingActionButton) findViewById(R.id.fabDirection);
        if(isAdmin){
            fabDirrection.setBackgroundColor(getResources().getColor(R.color.MediumPurple));
        }
        fabDirrection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMap;
                 if(gpsTracker.canGetLocation()){
                  intentMap = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+gpsTracker.getLatitude()+","+gpsTracker.getLongitude()+"&daddr="+hospLat+","+hospLng));
                  }
                else{
                   intentMap = new Intent(android.content.Intent.ACTION_VIEW,
                           Uri.parse("http://maps.google.com/maps?&daddr="+hospLat+","+hospLng));
              }

                startActivity(intentMap);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();




    }
}
