package za.co.boloka.hospitalfinder.Objects;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin-123 on 2016-08-06.
 */
public class UserItem {
    String email;
    String name;
    String image;
    String type;
    //String joined;

    public UserItem(String email, String name, String image,String type) {
        this.email = email;
        this.name = name;
        this.image = image;
        this.type =type;
      //  this.joined = joined;
    }
    // Default constructor required for calls to DataSnapshot.getValue(UserItem.class)
    public UserItem() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();

        result.put("email", email);
        result.put("name", name);
        result.put("image", image);
        result.put("type", type);
     //   result.put("joined", joined);




        return result;
    }

}
