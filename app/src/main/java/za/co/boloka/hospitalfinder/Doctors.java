package za.co.boloka.hospitalfinder;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import za.co.boloka.hospitalfinder.Objects.DoctorItem;
import za.co.boloka.hospitalfinder.Objects.MainItem;

/**
 * Created by Admin-123 on 2016-08-02.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Doctors extends Fragment {
    RecyclerView mRecyclerView ;
    VerticalAdapterDoctors verticalAdapterDoctors;

    private DatabaseReference mDatabase;
    private FirebaseStorage storage ;
    StorageReference storageRef;
    String name;
    ChildEventListener childEventListener;
    DatabaseReference hospitalsRef;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_doctors, container, false);
        mRecyclerView =(RecyclerView)view.findViewById(R.id.recycler_view);

        verticalAdapterDoctors =new VerticalAdapterDoctors(getActivity().getApplicationContext());
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(horizontalLayoutManagaer);
        mRecyclerView.setAdapter(verticalAdapterDoctors);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        //final Firebase myFirebaseRef = new Firebase("https://blinding-inferno-2764.firebaseio.com/");

        // [START auth_state_listener]
        storage = FirebaseStorage.getInstance();

        storageRef = storage.getReferenceFromUrl("gs://hospital-finder-72178.appspot.com");


        hospitalsRef = mDatabase.child("Hospitals");




        hospitalsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                name =dataSnapshot.getKey();
                Log.d("boloka", "onChildAdded: Hospials" + dataSnapshot.getKey());
                for (final DataSnapshot dataSnapshot1 :dataSnapshot.getChildren()){
                    Log.d("boloka", "onChildAdded: Hospials2" + dataSnapshot.getKey());
                    DatabaseReference docRef = hospitalsRef.child(dataSnapshot1.getKey()).child("Doctors");

                    docRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Log.d("boloka", "onChildAdded: Hospials3" + dataSnapshot.getKey());
                            for(DataSnapshot dataSnapshot2 :dataSnapshot.getChildren()){
                                DoctorItem doctorItem;
                                Log.d("boloka", "onChildAdded: Hospials4" + dataSnapshot2.getKey());
                                doctorItem = new DoctorItem(dataSnapshot2.child("image").getValue().toString(),dataSnapshot2.child("name").getValue().toString(),dataSnapshot2.child("field").getValue().toString(),dataSnapshot2.child("avaliable").getValue().toString(), name);
                                verticalAdapterDoctors.addDataItem(doctorItem);
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        return view;
    }



    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();



    }

}
