package za.co.boloka.hospitalfinder;

/**
 * Created by Admin-123 on 2016-08-02.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Stackle_002 on 2016-07-18.
 */
public class VerticalAdapterServices extends RecyclerView.Adapter<VerticalAdapterServices.MyViewHolder> {
    HashMap<String,ArrayList<String>> listFromFire= new HashMap<>();
    private ArrayList<String> verticalList=new ArrayList<>();
    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public de.hdodenhof.circleimageview.CircleImageView imgView;

        public TextView textView;

        public MyViewHolder(View view) {
            super(view);
textView =(TextView)view.findViewById(R.id.textViewService);


        }
    }


    public VerticalAdapterServices(Context context) {

        this.context =context;


    }


    public VerticalAdapterServices(){

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.services_items, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.textView.setText(verticalList.get(position));


      /*  displayImageOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .resetViewBeforeLoading(true)
                .showImageOnFail(context .getResources().getDrawable(R.drawable.em))
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.Builder(context)
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .build();
        imageLoader.init(configuration);*/
        // imageLoader.displayImage(verticalList.get(position) , holder.imgView, displayImageOptions);


    }
    public void addData(ArrayList<String> images){
        this.verticalList.addAll(images);
        ///   notifyDataSetChanged();
    }
    public void addData2(String images){
        this.verticalList.add(images);
        notifyDataSetChanged();
    }
    public void addDataChild(HashMap<String,ArrayList<String>>images){
        this.listFromFire=images;
        //  notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return verticalList.size();
    }
}