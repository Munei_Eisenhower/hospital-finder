package za.co.boloka.hospitalfinder;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import za.co.boloka.hospitalfinder.Objects.CropOption;

public class AdminAddDoctor extends AppCompatActivity {
    String Dkey;
    EditText docName;
    EditText docField;
    int PIC_Upload_Logo=3;
    final int PIC_CROP = 2;
    private Uri fileUri;
    String name;
    String field;
    String status;
    String image="";
    Spinner avSpinner;
    String UID;
    String storeHomeKey="";
    de.hdodenhof.circleimageview.CircleImageView proPic;
    private DatabaseReference mDatabase;
    private FirebaseStorage storage ;
    StorageReference storageRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_doctor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Doctor");
        mDatabase = FirebaseDatabase.getInstance().getReference();
     storage = FirebaseStorage.getInstance();
   storageRef = storage.getReferenceFromUrl("gs://hospital-finder-72178.appspot.com");

        Intent intent = getIntent();
        storeHomeKey = intent.getStringExtra("Hospital_ID");

docName =(EditText)findViewById(R.id.txtDoctorName);
        docField =(EditText)findViewById(R.id.txtDocField) ;
       avSpinner = (Spinner) findViewById(R.id.av_spinner);
        proPic =(de.hdodenhof.circleimageview.CircleImageView)findViewById(R.id.DocPic);

        final ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(this, R.array.av_array,
                        android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        proPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(
                            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    //we will handle the returned data in onActivityResult
                    i.putExtra("return-data", true);
                    startActivityForResult(Intent.createChooser(i,
                            "Complete action using"), PIC_Upload_Logo);
                } catch (ActivityNotFoundException anfe) {
                    //display an error message
                    String errorMessage = "Whoops - your device doesn't support this!";
                    Toast toast = Toast.makeText(AdminAddDoctor.this, errorMessage, Toast.LENGTH_SHORT);
                    toast.show();
                }





            }
        });

        // Apply the adapter to the spinner
        avSpinner.setAdapter(staticAdapter);
        UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(image.length()<4){
                    Snackbar.make(view, "Picture is not uploaded yet.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                else {
                    setText();

                    AlertDialog.Builder builder = new AlertDialog.Builder(AdminAddDoctor.this);
                    builder
                            .setTitle("Doctor added Successful.")
                            .setMessage("Do you want to add another Doctor?\n")


                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    docField.setText("");
                                    docName.setText("");
                                    Dkey = "";
                                    image="";
                                    proPic.setImageDrawable(getResources().getDrawable(R.drawable.placeholder));
                                    return;

                                }
                            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            AdminAddDoctor.this.finish();
                            startActivity(new Intent(AdminAddDoctor.this, MainActivity.class));

                        }
                    })
                            //Do nothing on no
                            .show();

                }
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (data != null) {


            if (requestCode == PIC_Upload_Logo) {

                fileUri = data.getData();


                doCrop(PIC_Upload_Logo); //   performCrop();
            } else if (requestCode == PIC_CROP) {


                ArrayList<String> l = new ArrayList<String>();



                Bundle extras = data.getExtras();



                if (extras != null) {
                    Bitmap photo = extras.getParcelable("data");



                    proPic.setImageBitmap(photo);


                    // key2 =    mDatabase.child("mystores").push().getKey();
                  Dkey = mDatabase.child("Hospitals").child(storeHomeKey).push().getKey();

                    StorageReference logopathref = storageRef.child("Hospitals/"+storeHomeKey+"/Doctors/"+Dkey+".jpg");
                    //      StorageReference logoRef = logopathref.child(storeHomeKey+".jpg");

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] dataFire = baos.toByteArray();

                    UploadTask uploadTask = logopathref.putBytes(dataFire);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Log.v("upload zwino","failedddd home:"+exception);
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                            Uri downloadUrl = taskSnapshot.getDownloadUrl();

                                 image =taskSnapshot.getStorage().getPath();
                            Log.v("upload zwino","kheyi home"+taskSnapshot.getStorage().toString());
                            Toast.makeText( AdminAddDoctor.this, "Done Uploading you can click to submit", Toast.LENGTH_SHORT).show();

                        }
                    });


                }
            }
        }

    }
    private void doCrop( int extra) {
        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        List<ResolveInfo> list =  this.getPackageManager().queryIntentActivities(intent, 0);

        int size = list.size();

        if (size == 0) {
            Toast.makeText( this, "Can not find image crop app", Toast.LENGTH_SHORT).show();


            return;
        } else {
            intent.setData(fileUri);

            intent.putExtra("outputX", 200);
            intent.putExtra("outputY", 200);
            intent.putExtra("aspectX", 0);
            intent.putExtra("aspectY", 0);
            intent.putExtra("scale", true);

            intent.putExtra("return-data", true);

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                try {
                    startActivityForResult(i, PIC_CROP);
                }catch (Exception e){
                    Toast.makeText(this, "Can not load image crop app", Toast.LENGTH_SHORT).show();
                }
            } else {
                try {
                    startActivityForResult(intent, PIC_CROP);
                }catch (Exception e){
                    Toast.makeText( this, "Can not load image crop apps", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    public void setText(){
        if(!validate()){
            return;
        }else {
           name =docName.getText().toString();
           field = docField.getText().toString();
           status = avSpinner.getSelectedItem().toString();

            AddDoctor(storeHomeKey, name, field,image, status );
        }
    }

    public boolean validate() {
        boolean valid = true;

        String name = docName.getText().toString();
        String field = docField.getText().toString();



        if (name.isEmpty() || name.length() < 3) {
            docName.setError("at least 3 characters");
            valid = false;
        } else {
           docName.setError(null);
        }
        if (field.isEmpty() ||field.length() < 3) {
            docField.setError("at least 3 characters");
            valid = false;
        } else {
            docField.setError(null);
        }


        if(image.isEmpty()||image.length()<3){
            Toast.makeText(AdminAddDoctor.this,"Please upload a picture",Toast.LENGTH_LONG).show();
            valid=false;
        }




        return valid;
    }
    public void AddDoctor(String key,String Dname,String Dfield,String Dimag,String Dstatus  ){
        DatabaseReference hosp=  mDatabase.child("Hospitals").child(key);

        hosp.child("Doctors").child(Dkey).child("name").setValue(Dname);
        hosp.child("Doctors").child(Dkey).child("field").setValue(Dfield);
        hosp.child("Doctors").child(Dkey).child("image").setValue(Dimag);
        hosp.child("Doctors").child(Dkey).child("avaliable").setValue(Dstatus);








    }


}
