package za.co.boloka.hospitalfinder.Objects;

/**
 * Created by Admin-123 on 2016-08-03.
 */
public class DoctorItem {

    String doctorImage;
    String doctorName;
    String doctorStatus;
    String doctorField;
    String doctorHospital;

    public DoctorItem(String doctorImage, String doctorName,String doctorField, String doctorStatus, String doctorHospital) {
        this.doctorImage = doctorImage;
        this.doctorName = doctorName;
        this.doctorStatus = doctorStatus;
        this.doctorHospital = doctorHospital;
        this.doctorField = doctorField;
    }

    public String getDoctorField() {
        return doctorField;
    }

    public void setDoctorField(String doctorField) {
        this.doctorField = doctorField;
    }

    public void setDoctorImage(String doctorImage) {
        this.doctorImage = doctorImage;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public void setDoctorStatus(String doctorStatus) {
        this.doctorStatus = doctorStatus;
    }

    public void setDoctorHospital(String doctorHospital) {
        this.doctorHospital = doctorHospital;
    }

    public String getDoctorImage() {
        return doctorImage;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public String getDoctorStatus() {
        return doctorStatus;
    }

    public String getDoctorHospital() {
        return doctorHospital;
    }
}
