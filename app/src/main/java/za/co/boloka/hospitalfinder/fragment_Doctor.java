package za.co.boloka.hospitalfinder;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class fragment_Doctor extends Fragment {

    RecyclerView mRecyclerView ;
    VerticalAdapterDoctors verticalAdapter;
    public fragment_Doctor() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_fragment__doctor, container, false);

        mRecyclerView =(RecyclerView)view.findViewById(R.id.recycler_view);
verticalAdapter = new HospitalDetails().verticalAdapterDoctors;
      //  verticalAdapter =new VerticalAdapterDoctors(getActivity().getApplicationContext(),10);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);


        mRecyclerView.setLayoutManager(horizontalLayoutManagaer);
        mRecyclerView.setAdapter(verticalAdapter);


        return view;
    }

}
